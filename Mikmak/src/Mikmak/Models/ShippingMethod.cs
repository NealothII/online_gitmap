﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Mikmak.Models
{
    public partial class ShippingMethod
    {
        public ShippingMethod()
        {
        
        }
        [Required]
        [MaxLength(256, ErrorMessage = "Name bestaat uit maximum 256 tekens")]
        public string Name { get; set; }
        [MaxLength(1024, ErrorMessage = "Name bestaat uit maximum 256 tekens")]
        public string Description { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Key]
        public int Id { get; set; }
    }
}
