﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikmak.Models
{
    public partial class OrderStatus
    {
        public OrderStatus()
        {
            Order = new HashSet<Order>();
        }
        [Required]
        [MaxLength(256, ErrorMessage ="Name bestaat uit max 256 karakters")]
        public string Name { get; set; }
        [MaxLength(1024, ErrorMessage = "Name bestaat uit max 256 karakters")]
        public string Description { get; set; }
        [Key]
        public int Id { get; set; }
        [NotMapped]
        public virtual ICollection<Order> Order { get; set; }
    }
}
