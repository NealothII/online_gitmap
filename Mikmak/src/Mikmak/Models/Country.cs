﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikmak.Models
{
    public partial class Country
    {
        public Country()
        {
           // Customer = new HashSet<Customer>();
           // Supplier = new HashSet<Supplier>();
        }
        [Required(ErrorMessage ="De kolom Code moet ingevuld zijn")]
        public string Code { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        [Required]
        [EmailAddress(ErrorMessage ="Verplicht een e-mail in te vullen.")]
        public string Name { get; set; }
        public double? ShippingCostMultiplier { get; set; }
        public int Id { get; set; }
        [NotMapped]
        public virtual IEnumerable<Country> List { get; set; }
    }
}
