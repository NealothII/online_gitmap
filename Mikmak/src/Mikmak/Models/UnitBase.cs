﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Mikmak.Models
{
    public partial class UnitBase
    {
        [Required]
        [MaxLength(256, ErrorMessage ="Name Bestaat uit maxmimum 256 karakters")]
        public string Name { get; set; }
        [MaxLength(1024, ErrorMessage = "Description Bestaat uit maxmimum 256 karakters")]
        public string Description { get; set; }
        public double? ShippingCostMultiplier { get; set; }
        [Required]
        [MaxLength(2, ErrorMessage ="Code Bestaat uit max 2 karakters")]
        public string Code { get; set; }
        [Key]
        public int Id { get; set; }
    }
}
