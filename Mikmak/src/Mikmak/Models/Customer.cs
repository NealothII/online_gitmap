﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikmak.Models
{
    public partial class Customer
    {
        public Customer()
        {
            Order = new HashSet<Order>();
        }
        [Required]
        [MaxLength(10, ErrorMessage = "NickName bestaat uit maximum 10 tekens.")]
        public string NickName { get; set; }
        [Required]
        [MaxLength(256, ErrorMessage = "FirstName bestaat uit maximum 256 tekens.")]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(256, ErrorMessage = "LastName bestaat uit maximum 256 tekens.")]
        public string LastName { get; set; }
        [Required]
        [MaxLength(256, ErrorMessage = "Address1 bestaat uit maximum 256 tekens.")]
        public string Address1 { get; set; }
        [MaxLength(256, ErrorMessage = "Address2 bestaat uit maximum 256 tekens.")]
        public string Address2 { get; set; }
        [Required]
        [MaxLength(256, ErrorMessage = "City bestaat uit maximum 256 tekens.")]
        public string City { get; set; }
        [MaxLength(80, ErrorMessage = "Region bestaat uit maximum 80 tekens.")]
        public string Region { get; set; }
        [Required]
        [MaxLength(20, ErrorMessage = "PostalCode bestaat uit maximum 20 tekens.")]
        public string PostalCode { get; set; }
        [Required]
        public int IdCountry { get; set; }
        [MaxLength(40, ErrorMessage = "Phone bestaat uit maximum 40 tekens.")]
        public string Phone { get; set; }
        [MaxLength(40, ErrorMessage = "Mobile bestaat uit maximum 40 tekens.")]
        public string Mobile { get; set; }
        [Key]
        public int Id { get; set; }
        [NotMapped]
        public virtual ICollection<Order> Order { get; set; }
    }
}
