﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikmak.Models
{
    public partial class Product
    {
        public Product()
        {
            OrderItem = new HashSet<OrderItem>();
        }
        [MaxLength(1024, ErrorMessage = "Description bestaat uit maximum 256 tekens")]
        public string Description { get; set; }
        [Required]
        [MaxLength(256, ErrorMessage = "Name bestaat uit maximum 256 tekens")]
        public string Name { get; set; }
        [Range(0, 999999999999,ErrorMessage ="Price moet tussen 0 - 99999999 liggen")]
        public double? Price { get; set; }
        [Range(0, 999999999999, ErrorMessage = "ShippingCost moet tussen 0 - 99999999 liggen")]
        public double? ShippingCost { get; set; }
        public int? TotalRating { get; set; }
        [MaxLength(256, ErrorMessage = "Thumbnail bestaat uit maximum 256 tekens")]
        public string Thumbnail { get; set; }
        public string Image { get; set; }
        public double? DiscountPercentage { get; set; }
        public int? Votes { get; set; }
        [Key]
        public int Id { get; set; }
        [NotMapped]
        public virtual ICollection<OrderItem> OrderItem { get; set; }
        public virtual Supplier Supplier { get; set; }
    }
}
