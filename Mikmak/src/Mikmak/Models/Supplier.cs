﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikmak.Models
{
    public partial class Supplier
    {
        public Supplier()
        {
        }
        [Required]
        [MaxLength(10, ErrorMessage = "Leverancierscode bestaat uit maximum 10 tekens.")]
        public string Code { get; set; }
        [Required]
        [MaxLength(256, ErrorMessage = "Naam bestaat uit maximum 256 tekens.")]
        public string Name { get; set; }
        [MaxLength(256, ErrorMessage = "Contact bestaat uit maximum 256 tekens.")]
        public string Contact { get; set; }
        [MaxLength(256, ErrorMessage = "Address bestaat uit maximum 256 tekens.")]
        public string Address { get; set; }
        [MaxLength(256, ErrorMessage = "City bestaat uit maximum 256 tekens.")]
        public string City { get; set; }
        [MaxLength(80, ErrorMessage = "Region bestaat uit maximum 80 tekens.")]
        public string Region { get; set; }
        [MaxLength(20, ErrorMessage = "PostalCode bestaat uit maximum 20 tekens.")]
        public string PostalCode { get; set; }
        [MaxLength(40, ErrorMessage = "Phone bestaat uit maximum 40 tekens.")]
        public string Phone { get; set; }
        [MaxLength(40, ErrorMessage = "Mobile bestaat uit maximum 40 tekens.")]
        public string Mobile { get; set; }
        public int CountryId { get; set; }

        [Key]
        public int Id { get; set; }
        [NotMapped]
        public IEnumerable<Country> CountryList { get; set; }

        public virtual Country Country { get; set; }
}
}
