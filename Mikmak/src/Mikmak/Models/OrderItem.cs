﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Mikmak.Models
{
    public partial class OrderItem
    {
        //public int IdProduct { get; set; }
        //public int IdOrder { get; set; }
        [Key]
        public int Id { get; set; }
        [Range(0, 9999999999999999.99)]
        public decimal? Quantity { get; set; }

        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; }
    }
}
