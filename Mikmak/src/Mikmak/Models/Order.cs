﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Mikmak.Models
{
    public partial class Order
    {
        public Order()
        {
            OrderItem = new HashSet<OrderItem>();
        }
        [DataType(DataType.DateTime, ErrorMessage ="Vul een datum in" )]
        public DateTime OrderDate { get; set; }
        [DataType(DataType.DateTime, ErrorMessage = "Vul een datum in")]
        public DateTime ShippingDate { get; set; }
        [MaxLength(512, ErrorMessage ="Comment bestaat uit max 512 karakters")]
        public string Comment { get; set; }
        [Key]
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ShippingMethodId { get; set; }
        public int StatusId { get; set; }

        public virtual ICollection<OrderItem> OrderItem { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ShippingMethod ShippingMethod { get; set; }
        public virtual OrderStatus Status { get; set; }
    }
}
