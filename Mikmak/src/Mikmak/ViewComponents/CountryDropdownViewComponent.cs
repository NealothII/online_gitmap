﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mikmak.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mikmak.ViewComponents
{
    public class CountryDropdownViewComponent : ViewComponent
    {
        private readonly MikmakDbContext _context;
        public CountryDropdownViewComponent(MikmakDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(int? countryId)
        {
            IEnumerable list = await _context.Country.ToListAsync();
            ViewBag.CountryId = countryId;
            return View(list);
        }
    }
}
