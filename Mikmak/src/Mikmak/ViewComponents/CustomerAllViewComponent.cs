﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mikmak.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mikmak.ViewComponents
{
    public class CustomerAllViewComponent : ViewComponent
    {
        private readonly MikmakDbContext _context;
        public CustomerAllViewComponent(MikmakDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var list = await _context.Customer.ToListAsync();
            var result = list.OrderBy(x => x.LastName);
            return View(result);
        }
    }
}
