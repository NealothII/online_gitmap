﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mikmak.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mikmak.ViewComponents
{
    public class OrderByCustomerViewComponent : ViewComponent
    {
        private readonly MikmakDbContext _context;

        public OrderByCustomerViewComponent(MikmakDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(int customerId)
        {
            IEnumerable list = await _context.Order.ToListAsync();
            ViewBag.CustomerId = customerId;
            return View(list);
        }
    }
}
