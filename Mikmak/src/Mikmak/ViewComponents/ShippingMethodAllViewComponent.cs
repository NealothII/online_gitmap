﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mikmak.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mikmak.ViewComponents
{
    public class ShippingMethodAllViewComponent : ViewComponent
    {

        private readonly MikmakDbContext _context;
        public ShippingMethodAllViewComponent(MikmakDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            IEnumerable list = await _context.ShippingMethod.ToListAsync();
            return View(list);
        }
    }
}
